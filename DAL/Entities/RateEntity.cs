﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EntityFramework.CodeFirst.Entities
{
    /// <summary>
    /// Entidad encargada de almacenar los datos de los "rates"
    /// </summary>
    public class RateEntity : BaseEntity
    { 
        public string From { get; set; }

        public string To { get; set; }

        public string Rate { get; set; }
    }
}
