﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EntityFramework.CodeFirst.Entities
{
    /// <summary>
    /// Entidad encargada de almacenar los datos de las "transacciones"
    /// </summary>
    public class TransactionEntity : BaseEntity
    {
        public string Sku { get; set; }

        public string Amount { get; set; }

        public string Currency { get; set; }
    }
}
