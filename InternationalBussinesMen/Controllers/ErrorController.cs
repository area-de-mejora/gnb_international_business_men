﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InternationalBussinesMen.Controllers
{
    public class ErrorController : Controller
    {
        /// <summary>
        /// Retornamos una vista genérica de errores
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}