﻿using EntityFramework.CodeFirst.Entities;
using InternationalBussinesMen.ApiClasses;
using InternationalBussinesMen.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace InternationalBussinesMen.Controllers
{
    public class HomeController : Controller
    {
        //Contexto de acceso a datos
        private EntityFramework.CodeFirst.ApplicationDbContext _context;

        public HomeController()
        {
            _context = new EntityFramework.CodeFirst.ApplicationDbContext();
        }


        #region Actions
        /// <summary>
        /// Método principal, que carga el Index con todas las opciones
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        /// <summary>
        /// Método GatRates, que retorna los rates desde el WS o desde B.D., en función la flag pasado como parámetro
        /// </summary>
        /// <param name="pWsEnabledFlag">Si es true, utiliza el WS; en caso contrario, emplea los datos de B.D: si los hubiese</param>
        /// <returns>Devuelve una vista con el listado XML de los rates</returns>
        public ActionResult GetRates(bool pWsEnabledFlag)
        {
            ResultsModels model = new ResultsModels();
            
            //Si el WS está activo, lo utilizamos
            if (pWsEnabledFlag == true)
            {
                //Url de la Api para obtener el xml con los rates
                string url = GetInternalUrl("/api/values/getratesandpersistinfo");

                //Xml string con los rates
                string strXmlResult = Utils.GetUrlContent(url); //Llamamos al WS api/values/getrates

                //Rellenamos el modelo con esos datos
                model.ConversionStrXmlListResult = strXmlResult;
            }
            else //Si el WS no está activo
            {
                //Obtenemos los datos de B.D.
                List<RateEntity> listRateEntity = _context.Rates.ToList();

                //Los mapeamos en las clases ApiXML
                RatesXml ratesXml = ParseRateEntityToRateXml(listRateEntity);

                //Obtenemos el xml string con los rates
                string strXmlResult = Utils.GetSerializedXMLStringFromObject(ratesXml, typeof(RatesXml));

                //Rellenamos el modelo
                model.ConversionStrXmlListResult = strXmlResult;
            }

            return View("Results", model);
        }

        /// <summary>
        /// Método GatTransactions, que retorna las transactions desde el WS o desde B.D., en función la flag pasado como parámetro
        /// </summary>
        /// <param name="pWsEnabledFlag">Si es true, utiliza el WS; en caso contrario, emplea los datos de B.D: si los hubiese</param>
        /// <returns>Devuelve una vista con el listado XML de las transactions</returns>
        public ActionResult GetTransactions(bool pWsEnabledFlag)
        {
            ResultsModels model = new ResultsModels();

            //Si el WS está activo, lo utilizamos
            if (pWsEnabledFlag == true)
            {
                //Url de la Api para obtener el xml con las transactions
                string url = GetInternalUrl("/api/values/gettransactionsandpersistinfo");

                //Xml string con las transactions
                string strXmlResult = Utils.GetUrlContent(url); //Llamamos al WS api/values/gettransactions

                //Rellenamos el modelo con esos datos
                model.ConversionStrXmlListResult = strXmlResult;
            }
            else //Si el WS no está activo
            {
                //Obtenemos los datos de B.D.
                List<TransactionEntity> listTransactionEntity = _context.Transactions.ToList();

                //Los mapeamos en las clases ApiXML
                TransactionsXml transactionsXml = ParseTransactionEntityToTransactionXml(listTransactionEntity);

                //Obtenemos el xml string con los rates
                string strXmlResult = Utils.GetSerializedXMLStringFromObject(transactionsXml, typeof(TransactionsXml));

                //Rellenamos el modelo
                model.ConversionStrXmlListResult = strXmlResult;
            }

            return View("Results", model);
        }

        /// <summary>
        /// Método GetConversionResult, que retorna las conversiones desde el WS o desde B.D., en función la flag pasado como parámetro
        /// </summary>
        /// <param name="pWsEnabledFlag">Si es true, utiliza el WS; en caso contrario, emplea los datos de B.D: si los hubiese</param>
        /// <param name="pSKU">Identificador de la transacción a buscar y convertir</param>
        /// <returns>Devuelve una vista con el listado XML de las transactions</returns>
        public ActionResult GetConversionResult(bool pWsEnabledFlag, string pSKU)
        {
            ResultsModels model = new ResultsModels();

            //Si el WS está activo, lo utilizamos
            if (pWsEnabledFlag == true)
            {
                //Url de la Api para obtener el xml con las transactions
                string url = GetInternalUrl(String.Format("/api/values/getconversionresult?pSKU={0}", pSKU));

                //Xml string con las transactions
                string strXmlResult = Utils.GetUrlContent(url); //Llamamos al WS api/values/gettransactions

                //Rellenamos el modelo con esos datos
                model.ConversionStrXmlListResult = strXmlResult;
            }
            else //Si el WS no está activo
            {
                //Obtenemos los datos de B.D.
                List<RateEntity> listRateEntity = _context.Rates.ToList();
                List<TransactionEntity> listTransactionEntity = _context.Transactions.ToList();

                //Los mapeamos en las clases ApiXML
                RatesXml ratesXml = ParseRateEntityToRateXml(listRateEntity);
                TransactionsXml transactionsXml = ParseTransactionEntityToTransactionXml(listTransactionEntity);

                //Obtenemos el xml string con los resultados convertidos en string
                string strXmlResult = Utils.GetStrConversionResult(pSKU, ratesXml, transactionsXml);

                //Rellenamos el modelo
                model.ConversionStrXmlListResult = strXmlResult;
            }

            return View("Results", model);
        }

        /// <summary>
        /// Método GetErrorLanding, que retorna la vista genérica de errores
        /// </summary>
        /// <returns>Devuelve la vista genérica de errores</returns>
        public ActionResult GetErrorLanding()
        {
            return View("Error");
        }

        #endregion


        #region Auxiliars

        /// <summary>
        /// Parsea de RateEntity a RateXml
        /// </summary>
        /// <param name="listRateEntity">Listado de entidades de B.D. a mapear</param>
        /// <returns>Retorna un objeto RatesXml con el listado de rates mapeadas</returns>
        public RatesXml ParseRateEntityToRateXml(List<RateEntity> listRateEntity)
        {
            RatesXml ret = new RatesXml();

            ret.Rate = new List<Rate>();            
            foreach (RateEntity rateEntityItem in listRateEntity)
            {
                Rate rateAux = new Rate();
                rateAux.From = rateEntityItem.From;
                rateAux.To = rateEntityItem.To;
                rateAux._rate = rateEntityItem.Rate;
                ret.Rate.Add(rateAux);
            }

            return ret;
        }

        /// <summary>
        /// Parsea de TransactionEntity a TransactionXml
        /// </summary>
        /// <param name="listTransactionEntity">Listado de entidades de B.D. a mapear</param>
        /// <returns>Retorna un objeto TransactionXml con el listado de transacciones mapeadas</returns>
        public TransactionsXml ParseTransactionEntityToTransactionXml(List<TransactionEntity> listTransactionEntity)
        {
            TransactionsXml ret = new TransactionsXml();

            ret.Transaction = new List<Transaction>();
            foreach (TransactionEntity transactionEntityItem in listTransactionEntity)
            {
                Transaction transactionAux = new Transaction();
                transactionAux.Sku = transactionEntityItem.Sku;
                transactionAux.Amount = transactionEntityItem.Amount;
                transactionAux.Currency = transactionEntityItem.Currency;
                ret.Transaction.Add(transactionAux);
            }

            return ret;
        }

        /// <summary>
        /// Método auxiliar para obtener una url interna usando el parámetro pasado
        /// </summary>
        /// <param name="pUrlPart">Parámetro string necesario para construir la Url</param>
        /// <returns>Retorna la URL construida</returns>
        public string GetInternalUrl(string pUrlPart)
        {
            string ret = "";

            ret = "http://" + Request.Url.Authority + pUrlPart;

            return ret;
        }

        #endregion

    }
}
