﻿using EntityFramework.CodeFirst;
using EntityFramework.CodeFirst.Entities;
using InternationalBussinesMen.ApiClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace InternationalBussinesMen.Controllers
{
    [Authorize]
    [RoutePrefix("api/values")]
    public class ValuesController : ApiController
    {
        //URL desde donde obtener los datos de los rates
        private string _ratesURL = "http://quiet-stone-2094.herokuapp.com/rates.xml";
        //URL desde donde obtener los datos de los rates
        private string _transactionsURL = "http://quiet-stone-2094.herokuapp.com/transactions.xml";
        //Contexto de acceso a datos
        private ApplicationDbContext _context;


        public ValuesController()
        {
            _context = new ApplicationDbContext();
        }


        #region Web Api actions

        /// <summary>
        /// Método GET del servicio Web Api, para obtener los datos de los rates, persistirlos en B.D y retornarlos
        /// </summary>
        /// <returns>Retorna un string con formato XML que contiene los datos de los rates de la URL configurada</returns>
        [AllowAnonymous]
        [Route("getratesandpersistinfo")] // GET api/values/getratesandpersistinfo
        public HttpResponseMessage GetRatesAndPersistInfo()
        {
            string strXML = String.Empty;

            //Descargamos el contenido de la URL
            strXML = Utils.GetUrlContent(_ratesURL);
            //TODO: Comentar, sólo para pruebas
            //strXML = Utils.GetUrlContent(@"FileFullPath");

            //Gestionamos la persistencia en B.D.
            ManageRateDbOperations(strXML);           

            //Lo retornamos con formato XML
            return new HttpResponseMessage()
            {
                Content = new StringContent(strXML, Encoding.UTF8, "application/xml")
            };
        }

        /// <summary>
        /// Método GET del servicio Web Api, para obtener los datos de las transacciones, persistirlos en B.D y retornarlos
        /// </summary>
        /// <returns>Retorna un string con formato XML que contiene los datos de las transacciones de la URL configurada</returns>        
        [AllowAnonymous]
        [Route("gettransactionsandpersistinfo")] // GET api/values/gettransactionsandpersistinfo
        public HttpResponseMessage GetTransactionsAndPersistInfo()
        {
            string strXML = String.Empty;

            //Descargamos el contenido de la URL
            strXML = Utils.GetUrlContent(_transactionsURL);
            //TODO: comentar, sólo para pruebas
            //strXML = Utils.GetUrlContent(@"FileFullPath");

            //Gestionamos la persistencia en B.D.
            ManageTransactionDbOperations(strXML);

            //Lo retornamos con formato XML
            return new HttpResponseMessage()
            {
                Content = new StringContent(strXML, Encoding.UTF8, "application/xml")
            };
        }

        /// <summary>
        /// Método GET del servicio Web Api, para obtener los datos de las conversiones y retornarlos, utilizando para ello el WS
        /// </summary>
        /// <param name="pSKU">Identificador de la transacción de la que obtener las conversiones</param>
        /// <returns>Retorna un string con formato XML que contiene los datos de las conversiones y de la suma de ellas</returns>        
        [AllowAnonymous]
        [Route("getconversionresult")] // GET api/values/getconversionresult?pSKU=XXXXX
        public HttpResponseMessage GetConversionResult(string pSKU)
        {
            //Retorno
            string retStr = String.Empty;

            ConversionResultXml conversionResultXmlAux = new ConversionResultXml();

            //Descargamos el contenido de la URL con los rates
            string strXmlRates = Utils.GetUrlContent(_ratesURL);
            //Deserializamos los datos XML en una estructura de objetos --> Aquí tenemos todas las conversiones disponibles
            RatesXml ratesXmlAux = GetRatesXml(strXmlRates);
            //Gestionamos la persistencia en B.D.
            ManageRateDbOperations(strXmlRates);


            //Descargamos el contenido de la URL con las transactions
            string strXmlTransactions = Utils.GetUrlContent(_transactionsURL);
            //Deserializamos los datos XML en una estructura de objetos --> Aquí tenemos todas las transacciones realizadas
            TransactionsXml transactionsXmlAux = GetTransactionsXml(strXmlTransactions);
            //Gestionamos la persistencia en B.D.
            ManageTransactionDbOperations(strXmlTransactions);

            retStr = Utils.GetStrConversionResult(pSKU, ratesXmlAux, transactionsXmlAux);

            //Lo retornamos con formato XML
            return new HttpResponseMessage()
            {
                Content = new StringContent(retStr, Encoding.UTF8, "application/xml")
            };
        }

        #endregion



        #region Database Operations

        /// <summary>
        /// Borramos e insertamos los nuevos datos de rates
        /// </summary>
        /// <param name="pStrXml">String Xml con los datos de los rates a insertar</param>
        public void ManageRateDbOperations(string pStrXml)
        {
            //Eliminamos los datos de Rates que tengamos en B.D.
            List<RateEntity> ratesToDelete = _context.Rates.ToList();
            _context.Rates.RemoveRange(ratesToDelete);
            _context.SaveChanges();

            //Obtenemos el listado de RateEntities preparado para insertarlo en B.D.
            List<RateEntity> rateEntityList = GetRateEntitiesParsed(pStrXml);

            //Persistimos la información en B.D.
            _context.Rates.AddRange(rateEntityList);
            _context.SaveChanges();
        }

        /// <summary>
        /// Se le pasa un string XML con datos de rates y lo retorna parseado a un listado de RateEntity
        /// </summary>
        /// <param name="pStrXml">string con los datos de los rates en Xml</param>
        /// <returns>Listado de RateEntity parseadas</returns>
        public List<RateEntity> GetRateEntitiesParsed(string pStrXml)
        {
            List<RateEntity> retRateEntityList = new List<RateEntity>();

            //Deserializamos los datos XML en una estructura de objetos
            RatesXml ratesXml = GetRatesXml(pStrXml);

            //Parseamos desde la clase-xml a entity
            foreach (Rate rateXmlItem in ratesXml.Rate)
            {
                RateEntity rateEntityAux = new RateEntity();
                rateEntityAux.From = rateXmlItem.From;
                rateEntityAux.Rate = rateXmlItem._rate;
                rateEntityAux.To = rateXmlItem.To;

                retRateEntityList.Add(rateEntityAux);
            }

            return retRateEntityList;
        }

        /// <summary>
        /// Partiendo de un listado string XML con datos de rates, deserializa esta información en un objeto RatesXml con el listado de rates y lo retorna
        /// </summary>
        /// <param name="pStrXml">String XML con los datos de rates</param>
        /// <returns>Objeto con los datos del Xml desearializados</returns>
        public RatesXml GetRatesXml(string pStrXml)
        {
            RatesXml retRatesXml = new RatesXml();

            if (!String.IsNullOrEmpty(pStrXml))
            {
                retRatesXml = (RatesXml)Utils.GetDeserializedObjectFromXMLString(pStrXml, typeof(RatesXml));
            }

            return retRatesXml;
        }


        /// <summary>
        /// Borramos e insertamos los nuevos datos de transactions
        /// </summary>
        /// <param name="pStrXml">String Xml con los datos de las transactions a insertar</param>
        public void ManageTransactionDbOperations(string pStrXml)
        {
            //Eliminamos los datos de Transactions que tengamos en B.D.
            List<TransactionEntity> transactionsToDelete = _context.Transactions.ToList();
            _context.Transactions.RemoveRange(transactionsToDelete);
            _context.SaveChanges();

            //Obtenemos el listado de transactionEntities preparado para insertarlo en B.D.
            List<TransactionEntity> transactionEntityList = GetTransactionEntitiesParsed(pStrXml);

            //Persistimos la información en B.D.
            _context.Transactions.AddRange(transactionEntityList);
            _context.SaveChanges();
        }

        /// <summary>
        /// Se le pasa un string XML con datos de transactions y lo retorna parseado a un listado de TransactionEntity
        /// </summary>
        /// <param name="pStrXml">string con los datos de las transacciones en Xml</param>
        /// <returns>Listado de TransactionEntity parseadas</returns>
        public List<TransactionEntity> GetTransactionEntitiesParsed(string pStrXml)
        {
            List<TransactionEntity> retTransactionEntityList = new List<TransactionEntity>();

            //Deserializamos los datos XML en una estructura de objetos
            TransactionsXml transactionsXml = GetTransactionsXml(pStrXml);

            //Parseamos desde la clase-xml a entity
            foreach (Transaction transactionXmlItem in transactionsXml.Transaction)
            {
                TransactionEntity transactionEntityAux = new TransactionEntity();
                transactionEntityAux.Sku = transactionXmlItem.Sku;
                transactionEntityAux.Amount = transactionXmlItem.Amount;
                transactionEntityAux.Currency = transactionXmlItem.Currency;
                retTransactionEntityList.Add(transactionEntityAux);
            }

            return retTransactionEntityList;
        }

        /// <summary>
        /// Partiendo de un listado string XML con datos de transactions, deserializa esta información en un objeto TransactionXml con el listado de transacciones y lo retorna
        /// </summary>
        /// <param name="pStrXml">String XML con los datos de transactions</param>
        /// <returns>Objeto con los datos del Xml desearializados</returns>
        public TransactionsXml GetTransactionsXml(string pStrXml)
        {
            TransactionsXml retTransactionsXml = new TransactionsXml();

            if (!String.IsNullOrEmpty(pStrXml))
            {
                retTransactionsXml = (TransactionsXml)Utils.GetDeserializedObjectFromXMLString(pStrXml, typeof(TransactionsXml));
            }

            return retTransactionsXml;
        }

        #endregion
    }
}
