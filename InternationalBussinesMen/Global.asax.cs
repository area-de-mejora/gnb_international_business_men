﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace InternationalBussinesMen
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Obtenemos la excepción
            Exception exception = Server.GetLastError();
            //La registramos en los logs
            Utils._log.Error(exception);

            //Limpiamos buffer e "interceptamos" el manejo de la excepción para redirigir desde aquí a la nuestra página de error
            Response.Clear();
            Server.ClearError();
            Response.Redirect("/Error");
        }
    }
}
