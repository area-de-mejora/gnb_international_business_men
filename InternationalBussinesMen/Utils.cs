﻿using InternationalBussinesMen.ApiClasses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Serialization;

namespace InternationalBussinesMen
{
    public static class Utils
    {
        private static string _currencyToConvert = "EUR";
        internal static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Obtiene el contenido devuelto por una URL
        /// </summary>
        /// <param name="pUrl">URL de la que obtener el contenido</param>
        /// <returns>string con el contenido de la URL</returns>
        public static string GetUrlContent(string pUrl)
        {            
            string ret = String.Empty;

            using (var wc = new WebClient())
            {
                ret = wc.DownloadString(pUrl);
            }

            return ret;
        }

        /// <summary>
        /// Obtiene el contenido de un fichero
        /// </summary>
        /// <param name="pFilePathFull">Full path del fichero del que obtener los datos</param>
        /// <returns>string con el contenido de la URL</returns>
        public static string GetFileContent(string pFilePathFull)
        {
            string ret = String.Empty;

            using (StreamReader reader = new StreamReader(pFilePathFull))
            {
                // Read entire text file with ReadToEnd.
                ret = reader.ReadToEnd();
            }

            return ret;
        }


        /// <summary>
        /// Serializa el objeto pasado como parámetro, retornando un string con el contenido en XML
        /// </summary>
        /// <param name="p">La instancia del objeto</param>
        /// <param name="pType">el typeOf del objeto</param>
        /// <returns></returns>
        public static string GetSerializedXMLStringFromObject(object p, Type pType)
        {
            string retorno = "";
            StringBuilder sb = new StringBuilder();

            XmlSerializer xmlSer = new XmlSerializer(pType);

            using (StringWriter writer = new Utf8StringWriter())
            {
                xmlSer.Serialize(writer, p);
                sb.AppendLine(writer.ToString());
            }

            retorno = sb.ToString();
            return retorno;
        }

        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }



        /// <summary>
        /// Recibe un string con un XML, y lo deserializa en el tipo de objeto indicado, retornándolo
        /// </summary>
        /// <param name="dataToDeserialize"></param>
        /// <param name="vType"></param>
        /// <returns></returns>
        public static object GetDeserializedObjectFromXMLString(string dataToDeserialize, Type vType)
        {
            object responseObject;

            using (StringReader sr = new StringReader(dataToDeserialize))
            { 
                XmlSerializer xmlSer = new XmlSerializer(vType);
                responseObject = xmlSer.Deserialize(sr);
                sr.Close();
            }

            return responseObject;
        }


        /// <summary>
        /// Recibido un identificador de transacción, calcula todas las transacciones en euros, la suma, y devuelve 
        /// esos datos serializados en un string XML
        /// </summary>
        /// <param name="pSKU">Id de transacción de la que obtener los datos convertidos</param>
        /// <param name="pRatesXml">listado de rates con los que hacer los cálculos</param>
        /// <param name="pTransactionXml">listado de transactions con las que hacer los cálculos</param>
        /// <returns>retorna un string XML con los datos de las conversiones</returns>
        public static string GetStrConversionResult(string pSKU, RatesXml pRatesXml, TransactionsXml pTransactionXml)
        {
            string retStrXml = String.Empty;

            //TODO: COMENTAR --> lo ponemos porque los resultados son aleatorios
            //pSKU = pTransactionXml.Transaction.FirstOrDefault().Sku; 

            //Nos quedamos sólo con las transacciones filtradas por "SKU"
            List<Transaction> transactionsXmlBySKU = pTransactionXml.Transaction.Where(d => d.Sku == pSKU).ToList();

            //Obtenemos las transacciones en Euros y se las asignamos al objeto auxiliar
            ConversionResultXml conversionResultXmlAux = new ConversionResultXml();
            conversionResultXmlAux.transactionList = new TransactionsXml();
            conversionResultXmlAux.transactionList.Transaction = GetTransactionsConvertedIntoEUR(transactionsXmlBySKU, pRatesXml);

            //Aquí ya tenemos el listado de transacciones filtradas por "SKU" y convertidas a Euros, así que lo que hacemos es simplemente sumarlas
            conversionResultXmlAux.transactionResult = conversionResultXmlAux.transactionList.Transaction.Select(d => Decimal.Parse(d.Amount.Replace('.', ','))).Sum();

            //Serializamos el objeto para obtenerlo en string y poder retornarlo en XML
            retStrXml = Utils.GetSerializedXMLStringFromObject(conversionResultXmlAux, typeof(ConversionResultXml));

            return retStrXml;
        }

        /// <summary>
        /// Dato un listado de rates y transactions devuelve el listado de transacciones convertidos a euros
        /// </summary>
        /// <param name="pTransactions">Listado de transactios sobre los que realizar los cálculos</param>
        /// <param name="pRatesAux">listado de rates con los que realizar los cálculos</param>
        /// <returns>Listado de transactions convertidas a euros</returns>
        public static List<Transaction> GetTransactionsConvertedIntoEUR(List<Transaction> pTransactions, RatesXml pRatesAux)
        {
            List<Transaction> retorno = new List<Transaction>();

            foreach (Transaction transactionItem in pTransactions)
            {
                Transaction transactionToAdd = new Transaction();

                //Cálculos
                transactionToAdd.Sku = transactionItem.Sku;
                transactionToAdd.Currency = _currencyToConvert;
                transactionToAdd.Amount = CalculateAmount(transactionItem.Amount, transactionItem.Currency, pRatesAux).ToString(); //TODO: ¿conversiones?

                retorno.Add(transactionToAdd);
            }

            return retorno;
        }

        /// <summary>
        /// Calcula el valor de la transacción pasada, en Euros
        /// </summary>
        /// <param name="pAmount">amount/valor de la transacción</param>
        /// <param name="pCurrency">divisa en la que está la transacción</param>
        /// <param name="pRates">listado de conversiones</param>
        /// <returns></returns>
        private static decimal CalculateAmount(string pAmount, string pCurrency, RatesXml pRates)
        {
            decimal retorno = 0;

            //Convertimos el amount a decimal
            decimal amountAux = Decimal.Parse(pAmount.Replace('.', ',')); //OJO las conversiones, tener en cuenta los 2 decimales que piden

            //Si la divisa de la transacción es "Euros", retornamos el "amount" directamente
            if (pCurrency == _currencyToConvert)
            {
                retorno = amountAux;
            }
            else //Si la divisa de la transacción no es "Euros"
            {
                //Obtenemos el factor de conversión
                decimal conversionFactor = GetConversionFactor(pCurrency, pRates);
                //Multiplicamos el "amount" por el factor de conversión obtenido
                retorno = amountAux * conversionFactor;
            }

            retorno = Math.Round(retorno, 2, MidpointRounding.ToEven); //Redondeo Bank

            return retorno;
        }

        /// <summary>
        /// Obtiene el factor de conversión a aplicar a la transacción
        /// </summary>
        /// <param name="pCurrency">divisa en la que está la transacción</param>
        /// <param name="pRates">listado de conversiones</param>
        /// <returns></returns>
        private static decimal GetConversionFactor(string pCurrency, RatesXml pRates)
        {
            decimal retorno = 1;

            //Buscamos si existe ya la conversión
            Rate rateAux = pRates.Rate.Where(d => d.From == pCurrency && d.To == _currencyToConvert).FirstOrDefault();

            if (rateAux != null) //TODO: verificar null
            {
                retorno = Decimal.Parse(rateAux._rate.Replace('.', ','));
            }
            else
            {             
                List<string> rateToExcludedList = new List<string>();
                do
                {
                    //Obtenemos todos los datos de una conversión cuyo origen sea "pCurrency"
                    rateAux = pRates.Rate.Where(d => d.From == pCurrency && rateToExcludedList.Contains(d.To) == false).FirstOrDefault();
                    //Añadimos esa divisa a la lista de exclusiones, para no volver a filtrar por ella
                    rateToExcludedList.Add(pCurrency);
                    //Divisa a la que convierte, que actualiza el valor de pCurrency para la siguiente iteración
                    pCurrency = rateAux.To;
                    //Valor de esa conversión
                    decimal rateFactor = Decimal.Parse(rateAux._rate.Replace('.', ','));
                    //Actualizamos el valor del factor de conversión final
                    retorno = retorno * rateFactor;
                }
                while (pCurrency != _currencyToConvert);
                
            }

            return retorno;
        }
    }
}