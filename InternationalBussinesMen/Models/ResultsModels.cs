﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternationalBussinesMen.Models
{
    public class ResultsModels
    {
        public decimal ConversionValueResult { get; set; }

        public string ConversionStrXmlListResult { get; set; }
    }
}