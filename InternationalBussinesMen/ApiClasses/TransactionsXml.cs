﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace InternationalBussinesMen.ApiClasses
{
    /// <summary>
    /// Clase auxiliar destinada a mapear los resultados de los datos XML de las transacciones
    /// </summary>
    [XmlRoot(ElementName = "transactions")]
    public class TransactionsXml
    {
        [XmlElement(ElementName = "transaction")]
        public List<Transaction> Transaction { get; set; }
    }


    [XmlRoot(ElementName = "transaction")]
    public class Transaction
    {
        [XmlAttribute(AttributeName = "sku")]
        public string Sku { get; set; }
        [XmlAttribute(AttributeName = "amount")]
        public string Amount { get; set; }
        [XmlAttribute(AttributeName = "currency")]
        public string Currency { get; set; }
    }
}