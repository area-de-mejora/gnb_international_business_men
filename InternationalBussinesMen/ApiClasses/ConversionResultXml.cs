﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InternationalBussinesMen.ApiClasses
{
    //Clase auxiliar destinada a mapear los resultados de los datos de las conversiones para renderizarlos posteriormente por pantalla
    public class ConversionResultXml
    {
        private decimal _transactionResult;
        public decimal transactionResult
        {
            get
            {
                return _transactionResult;
            }
            set
            {
                _transactionResult = value;
            }
        }

        private TransactionsXml _transactionList;
        public TransactionsXml transactionList
        {
            get
            {
                return _transactionList;
            }
            set
            {
                _transactionList = value;
            }
        }
    }
}