﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace InternationalBussinesMen.ApiClasses
{
    /// <summary>
    /// Clase auxiliar destinada a mapear los resultados de los datos XML de los rates
    /// </summary>
    [XmlRoot(ElementName = "rates")]
    public class RatesXml
    {
        [XmlElement(ElementName = "rate")]
        public List<Rate> Rate { get; set; }
    }

    [XmlRoot(ElementName = "rate")]
    public class Rate
    {
        [XmlAttribute(AttributeName = "from")]
        public string From { get; set; }
        [XmlAttribute(AttributeName = "to")]
        public string To { get; set; }
        [XmlAttribute(AttributeName = "rate")]
        public string _rate { get; set; }
    }
}